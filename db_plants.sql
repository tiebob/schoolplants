/*
SQLyog Community v12.01 (32 bit)
MySQL - 5.6.20 : Database - plants
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`plants` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `plants`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` varchar(1) COLLATE utf8_bin DEFAULT '1',
  `level` int(11) DEFAULT '1000',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `admin` */

insert  into `admin`(`id`,`username`,`password`,`active`,`level`,`created_date`) values (1,'adm','!2345678','0',1,NULL),(2,'user01','!2345','1',1000,NULL),(3,'user02','!23456','0',1000,NULL),(4,'user02','!23456','1',1000,NULL);

/*Table structure for table `plants_detail` */

DROP TABLE IF EXISTS `plants_detail`;

CREATE TABLE `plants_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `locate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url_link` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `plants_detail` */

insert  into `plants_detail`(`id`,`name`,`locate`,`type`,`url_link`,`created_date`) values (1,'小葉赤楠','前庭','喬木植物','aa1.htm',NULL),(2,'玉蘭花','前庭','喬木植物','a1.htm',NULL),(3,'苦楝','前庭','喬木植物','a2.htm',NULL),(4,'烏臼','前庭','喬木植物','a6.htm',NULL),(5,'楓香','前庭','喬木植物','a7.htm',NULL),(6,'榕樹','前庭','喬木植物','a9.htm',NULL),(7,'福木','前庭','喬木植物','a10.htm',NULL),(8,'鳳凰木','前庭','喬木植物','a11.htm',NULL),(9,'樟樹','前庭','喬木植物','a12.htm',NULL),(10,'緬梔','前庭','喬木植物','a13.htm',NULL),(11,'桂花','前庭','灌木植物','a4.htm',NULL),(12,'變葉木','前庭','灌木植物','a14.htm',NULL),(13,'萬年青','前庭','灌木植物','a8.htm',NULL),(14,'香蕉','前庭','草木植物','a3.htm',NULL),(15,'粉黛葉','前庭','草木植物','aa6.htm',NULL),(16,'大花紫薇','操場四周','喬木植物','e1.htm',NULL),(17,'阿勃勒','操場四周','喬木植物','ee5.htm',NULL),(18,'重陽木','操場四周','喬木植物','e9.htm',NULL),(19,'鳳凰木','操場四周','喬木植物','a11.htm',NULL),(20,'臺灣欒樹','操場四周','喬木植物','ee4.htm',NULL),(21,'麵包樹','操場四周','喬木植物','ee13.htm',NULL),(22,'茉莉花','操場四周','灌木植物','e8.htm',NULL),(23,'月橘','操場四周','灌木植物','ee2.htm',NULL),(24,'朱槿','操場四周','灌木植物','e4.htm\"',NULL),(25,'杜鵑花','操場四周','灌木植物','e5.htm',NULL),(26,'冇骨消','操場四周','灌木植物','ee3a.htm',NULL),(27,'日日春','操場四周','草木植物','e3.htm',NULL),(28,'兔兒菜','操場四周','草木植物','e6.htm',NULL),(29,'波斯菊','操場四周','草木植物','ea10.htm',NULL),(30,'馬利筋','操場四周','草木植物','e10.htm',NULL),(31,'蛇苺','操場四周','草木植物','e11.htm',NULL),(32,'紫花霍香薊','操場四周','草木植物','ee6.htm',NULL),(33,'紫背草','操場四周','草木植物','ee7.htm\"',NULL),(34,'菊花','操場四周','草木植物','ee8.htm',NULL),(35,'美人蕉','操場四周','草木植物','e7.htm',NULL),(36,'黃鵪菜','操場四周','草木植物','ee9.htm',NULL),(37,'落地生根','操場四周','草木植物','e14.htm',NULL),(38,'葉下珠','操場四周','草木植物','ee10.htm',NULL),(39,'鳶尾','操場四周','草木植物','e15.htm',NULL),(40,'蟛蜞菊','操場四周','草木植物','ee12.htm',NULL),(41,'水蜈蚣','操場四周','草木植物','ee3.htm',NULL),(42,'蕨類','操場四周',NULL,'ee11.htm',NULL),(43,'八重櫻','生態區','喬木植物','w69.html',NULL),(44,'九芎','生態區','喬木植物','w51.html',NULL),(45,'大葉山欖','生態區','喬木植物','f1.htm',NULL),(46,'大葉山欖','生態區','喬木植物','w71.html',NULL),(47,'文旦','生態區','喬木植物','w52.html',NULL),(48,'木棉','生態區','喬木植物','f3.htm\"',NULL),(49,'巴西鐵樹','生態區','喬木植物','w54.html',NULL),(50,'白千層','生態區','喬木植物','f6.htm',NULL),(51,'白千層','生態區','喬木植物','w59.html',NULL),(52,'白肉榕','生態區','喬木植物','w90.html',NULL),(53,'羊蹄甲','生態區','喬木植物','f8.htm',NULL),(54,'羊蹄甲','生態區','喬木植物','w88.html',NULL),(55,'吉野櫻','生態區','喬木植物','w124.html',NULL),(56,'杏','生態區','喬木植物','w128.html',NULL),(57,'芒果','生態區','喬木植物','f11.htm',NULL),(58,'青楓','生態區','喬木植物','w67.html',NULL),(59,'咖啡','生態區','喬木植物','w95.html',NULL),(60,'油桐','生態區','喬木植物','w106.html',NULL),(61,'茄冬','生態區','喬木植物','w92.html',NULL),(62,'垂葉女貞','生態區','喬木植物','w117.html',NULL),(63,'扁柏','生態區','喬木植物','f17.htm',NULL),(64,'美人樹','生態區','喬木植物','w78.html',NULL),(65,'流蘇','生態區','喬木植物','f18.htm',NULL),(66,'重陽木','生態區','喬木植物','f19.htm',NULL),(67,'風箱樹','生態區','喬木植物','w30.html',NULL),(68,'相思樹','生態區','喬木植物','w102.html',NULL),(69,'紅楠','生態區','喬木植物','w64.html',NULL),(70,'珊瑚刺桐','生態區','喬木植物','w53.html',NULL),(71,'烏桕','生態區','喬木植物','a6.htm',NULL),(72,'烏臼','生態區','喬木植物','w18.html',NULL),(73,'桑樹','生態區','喬木植物','w81.html',NULL),(74,'梅','生態區','喬木植物','w125.html',NULL),(75,'側柏','生態區','喬木植物','w72.html',NULL),(76,'細葉欖仁','生態區','喬木植物','b12.htm',NULL),(77,'黑板樹','生態區','喬木植物','f30.htm',NULL),(78,'黑板樹','生態區','喬木植物','w57.html',NULL),(79,'黃金風鈴木','生態區','喬木植物','f27.htm',NULL),(80,'黃脈刺桐','生態區','喬木植物','f28.htm',NULL),(81,'黃槐','生態區','喬木植物','f29.htm',NULL),(82,'菩提樹','生態區','喬木植物','f26.htm',NULL),(83,'棍棒椰子','生態區','喬木植物','w86.html',NULL),(84,'福木','生態區','喬木植物','f35.htm',NULL),(85,'福木','生態區','喬木植物','w80.html',NULL),(86,'榕樹','生態區','喬木植物','f33.htm',NULL),(87,'榕樹','生態區','喬木植物','w87.html',NULL),(88,'構樹','生態區','喬木植物','f34.htm',NULL),(89,'構樹','生態區','喬木植物','w60.html',NULL),(90,'銀杏','生態區','喬木植物','f37.htm',NULL),(91,'銀杏','生態區','喬木植物','w17.html',NULL),(92,'銀樺','生態區','喬木植物','w62.html',NULL),(93,'臺灣三角楓','生態區','喬木植物','w82.html',NULL),(94,'臺灣赤楠','生態區','喬木植物','w79.html',NULL),(95,'臺灣欒樹','生態區','喬木植物','w76.html',NULL),(96,'臺灣梭羅木','生態區','喬木植物','w61.html',NULL),(97,'蕃茉莉','生態區','喬木植物','w1.html',NULL),(98,'錫蘭橄欖','生態區','喬木植物','w45.html',NULL),(99,'澳洲鴨腳木','生態區','喬木植物','f38.htm',NULL),(100,'穗花棋盤腳','生態區','喬木植物','w50.html',NULL),(101,'薔薇','生態區','喬木植物','w84.html',NULL),(102,'龍眼','生態區','喬木植物','f39.htm',NULL),(103,'羅比親王海棗','生態區','喬木植物','f41.htm',NULL),(104,'羅比親王海棗','生態區','喬木植物','w68.html',NULL),(105,'羅漢松','生態區','喬木植物','w111.html',NULL),(106,'釋迦','生態區','喬木植物','f42.htm',NULL),(107,'麵包樹','生態區','喬木植物','w75.html',NULL),(108,'山芙蓉','生態區','灌木植物','w110.html',NULL),(109,'木槿','生態區','灌木植物','w110.html',NULL),(110,'巴西肉桂','生態區','灌木植物','w40.html',NULL),(111,'巴西牡丹','生態區','灌木植物','f2.htm',NULL),(112,'巴西野牡丹','生態區','灌木植物','w24.html',NULL),(113,'沙糖橘','生態區','灌木植物','w118.html',NULL),(114,'扶桑','生態區','灌木植物','f9.htm',NULL),(115,'杜鵑','生態區','灌木植物','f10.htm',NULL),(116,'金桔','生態區','灌木植物','f14.htm',NULL),(117,'金露華','生態區','灌木植物','w116.html',NULL),(118,'食茱萸','生態區','灌木植物','w36.html',NULL),(119,'桂竹','生態區','灌木植物','w114.html',NULL),(120,'雪茄花','生態區','灌木植物','f23.htm',NULL),(121,'野牡丹','生態區','灌木植物','w46.html',NULL),(122,'軟枝黃蟬','生態區','灌木植物','w129.html',NULL),(123,'梔子花','生態區','灌木植物','w107.html',NULL),(124,'球蘭','生態區','灌木植物','w119.html',NULL),(125,'麻葉繡球(小手球)','生態區','灌木植物','w126.html',NULL),(126,'圓葉血桐','生態區','灌木植物','w94.html',NULL),(127,'翠蘆莉','生態區','灌木植物','f36.htm',NULL),(128,'瑪瑙珠','生態區','灌木植物','w56.html',NULL),(129,'觀音棕竹','生態區','灌木植物','w113.html',NULL),(130,'三白草','生態區','草木植物','w26.html',NULL),(131,'火球花','生態區','草木植物','f5.htm',NULL),(132,'文珠蘭','生態區','草木植物','w47.html',NULL),(133,'月桃','生態區','草木植物','w35.html',NULL),(134,'仙草','生態區','草木植物','w91.html',NULL),(135,'甘蔗','生態區','草木植物','w109.html',NULL),(136,'含羞草','生態區','草木植物','w105.html',NULL),(137,'金午時花','生態區','草木植物','f13.htm',NULL),(138,'虎斑月桃','生態區','草木植物','w38.html',NULL),(139,'孤挺花','生態區','草木植物','w77.html',NULL),(140,'非洲鳳仙','生態區','草木植物','w122.html',NULL),(141,'姑婆芋','生態區','草木植物','w97.html',NULL),(142,'青龍鳶尾','生態區','草木植物','w48.html',NULL),(143,'波斯菊','生態區','草木植物','f12.htm',NULL),(144,'馬利筋','生態區','草木植物','89.html',NULL),(145,'食茱萸','生態區','草木植物','w36.html',NULL),(146,'香堇菜','生態區','草木植物','w120.html',NULL),(147,'咸豐草','生態區','草木植物','f16.htm',NULL),(148,'射干','生態區','草木植物','w21.html',NULL),(149,'海棠','生態區','草木植物','w121.html',NULL),(150,'彩葉草','生態區','草木植物','w100.html',NULL),(151,'紫花藿香薊','生態區','草木植物','f24.htm',NULL),(152,'黃龍草','生態區','草木植物','w43.html',NULL),(153,'紫背鴨拓草','生態區','草木植物','w127.html',NULL),(154,'紫蘇','生態區','草木植物','w83.html',NULL),(155,'葱蘭','生態區','草木植物','w93.html',NULL),(156,'矮牽牛','生態區','草木植物','w123.html',NULL),(157,'龍葵','生態區','草木植物','w85.html',NULL),(158,'薑','生態區','草木植物','f40.htm',NULL),(159,'檳榔心芋','生態區','草木植物','w49.html',NULL),(160,'水竹芋','生態區','草木植物','w33.html',NULL),(161,'水金英','生態區','草木植物','w34.html',NULL),(162,'水鱉','生態區','草木植物','w6.html',NULL),(163,'水蠟燭','生態區','草木植物','w7.html',NULL),(164,'水芙蓉','生態區','草木植物','w13.html',NULL),(165,'水稻','生態區','草木植物','w33.html',NULL),(166,'布袋蓮','生態區','草木植物','w34.html',NULL),(167,'印度杏菜','生態區','草木植物','w6.html',NULL),(168,'空心蔡','生態區','草木植物','w7.html',NULL),(169,'粉綠狐尾藻','生態區','草木植物','w13.html',NULL),(170,'野薑花','生態區','草木植物','w10.html',NULL),(171,'荷花','生態區','草木植物','w2.html',NULL),(172,'象耳澤瀉','生態區','草木植物','w104.html',NULL),(173,'無根萍','生態區','草木植物','w11.html',NULL),(174,'筊白筍','生態區','草木植物','f21.htm',NULL),(175,'黃花藺','生態區','草木植物','w8.html',NULL),(176,'臺灣萍蓬草','生態區','草木植物','w12.html',NULL),(177,'槐葉蘋','生態區','草木植物','w28.html',NULL),(178,'睡蓮','生態區','草木植物','w14.html',NULL),(179,'睡竽','生態區','草木植物','w9.html',NULL),(180,'特登慈菇','生態區','草木植物','w32.html',NULL),(181,'輪傘草','生態區','草木植物','w103.html',NULL),(182,'澤瀉','生態區','草木植物','w3.html',NULL),(183,'龍骨瓣杏菜','生態區','草木植物','w4.html',NULL),(184,'地錦','生態區','藤本植物','w5.html',NULL),(185,'金銀花','生態區','藤本植物','w27.html',NULL),(186,'牽牛花','生態區','藤本植物','w65.html',NULL),(187,'紫滕','生態區','藤本植物','w25.html',NULL),(188,'華它卡藤','生態區','藤本植物','w31.html',NULL),(189,'葡萄(巨峰葡萄)','生態區','藤本植物','w101.html',NULL),(190,'蒜香藤','生態區','藤本植物','w112.html',NULL),(191,'木瓜','生態區','喬木植物','w108.html',NULL),(192,'九重葛','後花園','喬木植物','b1.htm',NULL),(193,'木棉','後花園','喬木植物','b2.htm',NULL),(194,'櫻花','後花園','喬木植物','b19.htm',NULL),(195,'麵包樹','後花園','喬木植物','ee13.htm',NULL),(196,'黑板樹','後花園','喬木植物','f30.htm',NULL),(197,'台灣三角楓','後花園','喬木植物','b4.htm',NULL),(198,'芭樂','後花園','喬木植物','d4.htm',NULL),(199,'茶花','後花園','灌木植物','b10.htm',NULL),(200,'玉棠春','後花園','灌木植物','b5.htm',NULL),(201,'馬纓丹','後花園','灌木植物','b11.htm',NULL),(202,'仙丹花','後花園','灌木植物','b3.htm',NULL),(203,'朱槿','後花園','灌木植物','b6.htm',NULL),(204,'桂花','後花園','灌木植物','a4.htm',NULL),(205,'金露花','後花園','灌木植物','b8.htm',NULL),(206,'辣椒','後花園','灌木植物','d7.htm',NULL),(207,'咸豐草','後花園','草木植物','b9.htm',NULL),(208,'射干','後花園','草木植物','bb9.htm',NULL),(209,'假人蔘','後花園','草木植物','bb11.htm',NULL),(210,'魚腥草','後花園','草木植物','b13.htm',NULL),(211,'紫花酢醬草',NULL,'草木植物','b14.htm',NULL),(212,'紫蘇',NULL,'草木植物','b15.htm',NULL),(213,'黃花酢醬草',NULL,'草木植物','b16.htm',NULL),(214,'翠蘆莉',NULL,'草木植物','b18.htm',NULL),(215,'鴨跖草',NULL,'草木植物','ba18.htm',NULL),(216,'龍葵',NULL,'草木植物','bb18.htm',NULL),(217,'孤挺花',NULL,NULL,'b7.htm',NULL);

/*Table structure for table `plants_locate` */

DROP TABLE IF EXISTS `plants_locate`;

CREATE TABLE `plants_locate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `plants_locate` */

insert  into `plants_locate`(`id`,`name`,`created_date`) values (1,'前庭',NULL),(2,'操場四周',NULL),(3,'鐵金剛區',NULL),(4,'後花園',NULL);

/*Table structure for table `plants_type` */

DROP TABLE IF EXISTS `plants_type`;

CREATE TABLE `plants_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `plants_type` */

insert  into `plants_type`(`id`,`name`,`created_date`) values (1,'喬木植物',NULL),(2,'灌木植物',NULL),(3,'草本植物',NULL),(4,'藤本植物',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
